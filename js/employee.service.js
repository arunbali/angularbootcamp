( function( angular ) {

	angular.module( 'employeeModule', [] )
		.service( 'employeeService', function( $http ) {
			this.getEmployees = function() {
				return $http.get( '/demo-data/employees.json' ).then( employeeResults );
			};

			function employeeResults( result ) {
				return result.data;
			}
		})
		.controller( 'employeeController', function( employeeService ) {
			var vm = this;
			vm.employeeList = {};

			vm.loadEmployees = function() {
				employeeService.getEmployees().then( function( data ) {
					vm.employeeList = data;
				} )
			}
		})

})( window.angular );